package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="encuesta")
public class Encuesta implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String tituloEncuesta;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String textoExplicacion;
	
	@NotEmpty(message="no puede estar vacío")
	@Size(min=5,max=5)
	@Column(nullable=false)
	private String codigoIndentifica;
	
	
	@Temporal(TemporalType.DATE)
	private Date fechaCreacion;
	
	@Temporal(TemporalType.DATE)
	private Date fechaInicio;
	
	@Temporal(TemporalType.DATE)
	private Date fechaFin;
	
	@PrePersist
	public void prePersist() {
		this.fechaCreacion=new Date();
	}
	
	private Integer usuarioId;
	
	
	/*@ManyToMany(cascade = CascadeType.ALL)
    private List<Usuario> usuarios;*/
	
	/*@JsonIgnoreProperties({"encuesta","hibernateLazyInitializer","handler"})
	@ManyToOne(fetch=FetchType.LAZY)
	private Usuario usuario;
	
	/*@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinColumn(name="encuesta_id")
	private List<ItemEncuesta> items;*/
	
	

	/*public List<Usuario> getUsuarios() {
		return usuarios;
	}



	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}



	public Encuesta() {
	
		this.usuarios = new ArrayList<>();
	}*/



	public String getCodigoIndentifica() {
		return codigoIndentifica;
	}

	public void setCodigoIndentifica(String codigoIndentifica) {
		this.codigoIndentifica = codigoIndentifica;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public String getTituloEncuesta() {
		return tituloEncuesta;
	}



	public void setTituloEncuesta(String tituloEncuesta) {
		this.tituloEncuesta = tituloEncuesta;
	}



	public String getTextoExplicacion() {
		return textoExplicacion;
	}



	public void setTextoExplicacion(String textoExplicacion) {
		this.textoExplicacion = textoExplicacion;
	}



	public String getCodigo() {
		return codigoIndentifica;
	}



	public void setCodigo(String codigo) {
		this.codigoIndentifica = codigo;
	}



	public Date getFechaCreacion() {
		return fechaCreacion;
	}



	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}



	public Date getFechaInicio() {
		return fechaInicio;
	}



	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}



	public Date getFechaFin() {
		return fechaFin;
	}



	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}





	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}

