package com.tino.springboot.backend.apirest.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.tino.springboot.backend.apirest.models.entity.Encuesta;
import com.tino.springboot.backend.apirest.models.entity.Usuario;
import com.tino.springboot.backend.apirest.models.services.IEncuestaService;

@CrossOrigin(origins = {"http://localhost:4200"})

@RestController
@RequestMapping("/api")
public class EncuestaRestController {
	
	@Autowired
	private IEncuestaService encuestaService;
	
	@GetMapping("/encuestas")
	public List<Encuesta> index() {
		return encuestaService.findAll();
	}
	
	@GetMapping("/encuestas/{id}")
	public Encuesta show(@PathVariable Long id) {
		return encuestaService.findById(id);
	}
	
	@PostMapping("/encuestas")
	@ResponseStatus(HttpStatus.CREATED)
	public Encuesta create(@RequestBody Encuesta encuesta) {
		return encuestaService.save(encuesta);
	}
	
	@PutMapping("/encuestas/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Encuesta update(@RequestBody Encuesta encuesta,@PathVariable Long id) {
		
		Encuesta encuestaActual=encuestaService.findById(id);
		
		encuestaActual.setTituloEncuesta(encuesta.getTituloEncuesta());
		encuestaActual.setTextoExplicacion(encuesta.getTextoExplicacion());
		encuestaActual.setCodigo(encuesta.getCodigo());
		encuestaActual.setFechaCreacion(encuesta.getFechaCreacion());
		encuestaActual.setFechaInicio(encuesta.getFechaInicio());
		encuestaActual.setFechaFin(encuesta.getFechaFin());
		
		return encuestaService.save(encuestaActual);
	}
	
	@DeleteMapping("/encuestas/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		encuestaService.delete(id);
	}
	
	

}
