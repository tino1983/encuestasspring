package com.tino.springboot.backend.apirest.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="usuarios")
public class Usuario implements Serializable {
	
	
  private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message="no puede estar vacío")
	@Size(min=4,max=12)
	@Column(nullable=false)
	private String nombre;
	
	@NotEmpty(message="no puede estar vacío")
	private String apellidos;
	
	@NotEmpty(message="no puede estar vacío")
	@Email
	@Column(nullable=false,unique=true)
	private String email;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String password;
	
	@NotEmpty(message="no puede estar vacío")
	@Column(nullable=false)
	private String userName;
	
	
	public String getUserName() {
		return userName;
	}



	public void setUserName(String userName) {
		this.userName = userName;
	}



	public String getPassword() {
		return password;
	}



	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name="fechaCreacion")
	@Temporal(TemporalType.DATE)
	private Date fechaCreacion;
	
	@PrePersist
	public void prePersist() {
		this.fechaCreacion=new Date();
	}
	
	/*@ManyToMany(mappedBy = "usuarios")
    private List<Encuesta> encuestas;*/
	
	/*@JsonIgnoreProperties({"usuario","hibernateLazyInitializer","handler"})
	@OneToMany(fetch=FetchType.LAZY, mappedBy="usuario", cascade= CascadeType.ALL)
	private List<Encuesta> encuestas;
	/* cascade nos sirve para que cuando se borre un usuario también se borren todas sus encuestas*/
	
	


	
	/*public Usuario() {
		this.encuestas = new ArrayList<>();
		
	}



	public List<Encuesta> getEncuestas() {
		return encuestas;
	}



	public void setEncuestas(List<Encuesta> encuestas) {
		this.encuestas = encuestas;
	}


*/








	public Long getId() {
		return id;
	}
	


	public void setId(long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateAt() {
		return fechaCreacion;
	}

	public void setCreateAt(Date createAt) {
		this.fechaCreacion = createAt;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	
	

}
