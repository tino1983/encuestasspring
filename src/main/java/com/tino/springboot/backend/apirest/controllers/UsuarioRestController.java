package com.tino.springboot.backend.apirest.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;


import com.tino.springboot.backend.apirest.models.entity.Usuario;

import com.tino.springboot.backend.apirest.models.services.IUsuarioService;

@CrossOrigin(origins = { "http://localhost:4200" }) /*
													 * Esta anotación sirve para conectar con Angular y que puedan
													 * intercambiar información(archivos,jscon,peticiones,etc)
													 */
@RestController
@RequestMapping("/api")
public class UsuarioRestController {

	@Autowired
	private IUsuarioService usuarioService;

	@GetMapping("/usuarios")
	public List<Usuario> index() {
		return usuarioService.findAll();
	}
	
	/*@GetMapping("/usuarios/{id}")
	public Usuario show(@PathVariable Long id) {
		return clienteService.findById(id);
	}*/
	@GetMapping("/usuarios/{id}")
	public ResponseEntity<?> show(@PathVariable Long id) {
		
		Usuario usuario=null;
		Map<String,Object> response = new HashMap<>();
		
		try {
			usuario=this.usuarioService.findById(id);
		} catch(DataAccessException e){
			response.put("mensaje", "Error al realizar la consulta en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		if(usuario==null) {
			response.put("mensaje", "El cliente id: ".concat(id.toString()).concat(" no existe en la base de datos"));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<Usuario>(usuario,HttpStatus.OK);
		
		
	}
	
	/*@PostMapping("/usuarios")
	@ResponseStatus(HttpStatus.CREATED)
	public Usuario create(@RequestBody Usuario usuario) {
		return usuarioService.save(usuario);
	}*/
	
	@PostMapping("/usuarios")

	public ResponseEntity<?> create(@Valid @RequestBody Usuario usuario, BindingResult result) {
		
		Usuario userNew = null;
		Map<String,Object> response = new HashMap<>();
		
		/* Con el siguiente if validamos los campos*/
		
		if(result.hasErrors()) {
			
			List<String> errors = new ArrayList<>();
			
			for (FieldError err:result.getFieldErrors() ) {
				errors.add("El campo ' "+ err.getField()+ "'" +err.getDefaultMessage());
			}
			
			/*List<String> errors = result.getFieldErrors().stream().map(err -> "El campo ' "+ err.getField()+ "'" +err.getDefaultMessage()).collect(Collectors.toList());*/
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
	
		
		/* con esta parte de código ponemos los mensajes de errores*/
		
		try {
			userNew=this.usuarioService.save(usuario);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la inserción en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","Inserción realizada con éxito");
		response.put("usuario", userNew);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}
	
	/*@PutMapping("/usuarios/{id}")
	@ResponseStatus(HttpStatus.OK)
	public Usuario update(@RequestBody Usuario usuario,@PathVariable Long id) {
		Usuario usuarioActual=usuarioService.findById(id);
		usuarioActual.setNombre(usuario.getNombre());
		usuarioActual.setApellido(usuario.getApellido());
		usuarioActual.setEmail(usuario.getEmail());
		
		return usuarioService.save(usuarioActual);
	}*/
	
	@PutMapping("/usuarios/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody Usuario usuario,BindingResult result,@PathVariable Long id) {
		Usuario usuarioActual=this.usuarioService.findById(id);
		
		
		Map<String,Object> response = new HashMap<>();
		
		if(result.hasErrors()) {
			List<String> errors = result.getFieldErrors().stream().map(err -> "El campo ' "+ err.getField()+ "'" +err.getDefaultMessage()).collect(Collectors.toList());
			
			response.put("errors", errors);
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.BAD_REQUEST);
		}
		
		if(usuarioActual==null) {
			response.put("mensaje", "El cliente id: ".concat(id.toString()).concat(" no existe en la base de datos"));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.NOT_FOUND);
		}
		
		try {
			usuarioActual.setNombre(usuario.getNombre());
			usuarioActual.setApellidos(usuario.getApellidos());
			usuarioActual.setEmail(usuario.getEmail());
			usuarioActual.setCreateAt(usuario.getCreateAt());
			usuarioActual.setPassword(usuario.getPassword());
			usuarioActual.setUserName(usuario.getUserName());
			
			 this.usuarioService.save(usuarioActual);
			
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la edición en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","Edición realizada con éxito");
		response.put("usuario", usuarioActual);
		
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}
	
	/*@DeleteMapping("/usuarios/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		usuarioService.delete(id);
	}*/
	
	@DeleteMapping("/usuarios/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		
		Map<String,Object> response = new HashMap<>();
		
		try {
			this.usuarioService.delete(id);
		} catch(DataAccessException e) {
			response.put("mensaje", "Error al realizar la eliminación en la base de datos");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String,Object>>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put("mensaje","Eliminación realizada con éxito");
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.OK);
	}

}
